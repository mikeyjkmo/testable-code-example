# Demonstration on how to write more testable code #

This is the code used for the demonstration in the following video: https://youtu.be/2b2-WPzWl6c

## Pre-Requisites ##

* Python 3
* Pip
* Python Poetry (optional)

## Installation ##

Standard:

```bash
> pip install -e .
```

or, using Poetry:

```bash
> poetry install
```

## Run Tests ##

```bash
> pytest
```

Or, using Poetry:

```bash
> poetry run pytest
```