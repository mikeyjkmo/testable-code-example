import json
import requests
from lxml import html


class AppContext:
    pass


def fetch_github_jobs():
    res = requests.get(
        'https://jobs.github.com/positions.json',
        params={"description": "python", "location": "london"}
    )
    return res.json()


def fetch_raw_response(url):
    return requests.get(url).text


def build_companies_summaries(jobs, company_webpages):
    results = []
    for job, webpage in zip(jobs, company_webpages):
        tree = html.fromstring(webpage)
        title = tree.find('.//title').text

        results.append({
            "name": job["company"],
            "summary": title,
        })

    return results


def get_company_summaries_for_jobs(context):
    # Step 1
    # Retrieve data from dependencies
    # Retrieve github jobs
    jobs = context.fetch_github_jobs()  # A function that takes no args, returns a list of jobs

    # Retrieve all web pages
    # fetch_raw_response is a function that takes a url and returns a string
    company_webpages = [context.fetch_raw_response(job["company_url"]) for job in jobs]

    # Step 2
    # Process and combine retrieved data
    return build_companies_summaries(jobs, company_webpages)


def production_context():
    context = AppContext()
    context.fetch_github_jobs = fetch_github_jobs
    context.fetch_raw_response = fetch_raw_response
    return context


if __name__ == "__main__":
    context = production_context()
    print(json.dumps(get_company_summaries_for_jobs(context), indent=2))
