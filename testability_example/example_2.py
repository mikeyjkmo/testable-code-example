import json
import requests
from lxml import html


def fetch_github_jobs():
    res = requests.get(
        'https://jobs.github.com/positions.json',
        params={"description": "python", "location": "london"}
    )
    return res.json()


def fetch_raw_response(url):
    return requests.get(url).text


def build_companies_summaries(jobs, company_webpages):
    results = []
    for job, webpage in zip(jobs, company_webpages):
        tree = html.fromstring(webpage)
        title = tree.find('.//title').text

        results.append({
            "name": job["company"],
            "summary": title,
        })

    return results


def get_company_summaries_for_jobs():
    # Step 1
    # Retrieve github jobs
    jobs = fetch_github_jobs()

    # Retrieve all web pages
    company_webpages = [fetch_raw_response(job["company_url"]) for job in jobs]

    # Step 2
    # Process and combine retrieved data
    return build_companies_summaries(jobs, company_webpages)


if __name__ == "__main__":
    print(json.dumps(get_company_summaries_for_jobs(), indent=2))
