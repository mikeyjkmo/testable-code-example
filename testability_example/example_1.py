import json
import requests
from lxml import html


def get_company_summaries_for_jobs():
    # Step 1
    # Retrieve data from dependencies
    res = requests.get(
        'https://jobs.github.com/positions.json',
        params={"description": "python", "location": "london"}
    )
    jobs = res.json()

    company_webpages = [requests.get(job["company_url"]).text for job in jobs]

    # Step 2
    # Process and combine retrieved data
    results = []
    for job, webpage in zip(jobs, company_webpages):
        tree = html.fromstring(webpage)
        title = tree.find('.//title').text

        results.append({
            "name": job["company"],
            "summary": title,
        })

    return results


if __name__ == "__main__":
    print(json.dumps(get_company_summaries_for_jobs(), indent=2))
