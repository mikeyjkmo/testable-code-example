import json
import requests
from lxml import html


def get_company_summaries_for_jobs():
    res = requests.get(
        'https://jobs.github.com/positions.json',
        params={"description": "python", "location": "london"}
    )
    jobs = res.json()

    results = []

    for job in jobs:
        res = requests.get(job["company_url"])
        tree = html.fromstring(res.text)
        title = tree.find('.//title').text

        results.append({
            "name": job["company"],
            "summary": title,
        })

    return results


if __name__ == "__main__":
    print(json.dumps(get_company_summaries_for_jobs(), indent=2))
