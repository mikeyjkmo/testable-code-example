import pytest
from testability_example.example_2 import (
    get_company_summaries_for_jobs,
    build_companies_summaries,
)


@pytest.mark.skip
class TestCompanyGetSummariesForJobsIntegration:
    """
    Integration tests for get_company_summaries_for_jobs
    """

    def test_returns_list(self):
        results = get_company_summaries_for_jobs()
        assert type(results) == list

    def test_returns_expected_element_format(self):
        """
        This is a fragile test, as Github may return no jobs...
        """
        results = get_company_summaries_for_jobs()
        result = results[0]

        assert "name" in result
        assert "summary" in result


class TestBuildCompaniesSummariesUnit:
    """
    Unit tests for build_companies_summaries
    """

    def test_returns_expected_name_and_summaries(self):
        # Given
        jobs = [
            {
                "id": "f61bcf90-e400-11e8-8ff5-71d2e066e687",
                "type": "Full Time",
                "created_at": "Fri Nov 09 09:23:05 UTC 2018",
                "company": "University of Cambridge",
                "company_url": "https://www.gen.cam.ac.uk/",
                "location": "Cambridge, UK",
                "title": "Research Software Engineer",
            },
            {
                "id": "5cfc5c04-d90f-11e8-9d50-d2e1d303c48e",
                "type": "Full Time",
                "created_at": "Fri Oct 26 11:08:15 UTC 2018",
                "company": "http://www.paddle.com",
                "company_url": "http://www.paddle.com",
                "location": "London",
                "title": "Senior Python Software Engineer ",
            },
        ]
        company_web_pages = [
            """
            <html>
              <head><title>Department of Genetics</title></head>
            </html>
            """,
            """
            <html>
              <head><title>Paddle - Sell Software Online with Paddle's SaaS Commerce Platform</title></head>
            </html>
            """,
        ]

        # When
        results = build_companies_summaries(jobs, company_web_pages)

        # Then
        expected_results = [
            {"name": "University of Cambridge", "summary": "Department of Genetics"},
            {
                "name": "http://www.paddle.com",
                "summary": "Paddle - Sell Software Online with Paddle's SaaS Commerce Platform",
            },
        ]
        assert results == expected_results
