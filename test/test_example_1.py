import pytest
from testability_example.example_1 import get_company_summaries_for_jobs


@pytest.mark.slow
class TestCompanyGetSummariesForJobsIntegration:
    """
    Integration tests for get_company_summaries_for_jobs
    """

    def test_returns_list(self):
        results = get_company_summaries_for_jobs()
        assert type(results) == list

    def test_returns_expected_element_format(self):
        """
        This is a fragile test, as Github may return no jobs...
        """
        results = get_company_summaries_for_jobs()
        result = results[0]

        assert 'name' in result
        assert 'summary' in result
